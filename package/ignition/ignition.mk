################################################################################
#
# ignition
#
################################################################################
IGNITION_VERSION = 92102d01414deba04131b1edd8f5665d5bd71828
IGNITION_SITE = https://github.com/alexjstubbs/ignition.git
IGNITION_SITE_METHOD = git
IGNITION_LICENSE = GPLv3+
IGNITION_DEPENDENCIES = host-pkgconf

define IGNITION_BUILD_CMDS
	$(MAKE) CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D) all
endef

define IGNITION_INSTALL_TARGET_CMDS
	$(MAKE) -C $(@D) DESTDIR=$(TARGET_DIR) install
endef

$(eval $(generic-package))
